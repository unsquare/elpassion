
function blurReposition() {
    blurPosition = $('.window').position();
    blurPosition.left = $('.window').css('margin-left');
    let pIndex = blurPosition.left.indexOf('p');
    blurPosition.both = "-" + (blurPosition.left.substring(0, pIndex) - blurOffset) + "px     -" + (blurPosition.top - blurOffset) + "px";
    $('.sidebar__bg-blur').css("background-position", blurPosition.both);
}

document.addEventListener("DOMContentLoaded", function (event) {
    $('.issue').click(function () {
        if ($(this).hasClass('issue--stared')) {
            $(this).removeClass('issue--stared');
        } else {
            $(this).addClass('issue--stared');
        }
    });
    blurReposition();
});